package org.siani.itrules.engine.formatters.spelling;

public interface WordSpelling {
    String spell(int number);
}
