package org.siani.itrules;

public enum LineSeparator {
    LF, CRLF
}
