package org.siani.itrules.intellij.lang;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class ItrulesIcons {


    public static Icon ICON_13 = IconLoader.getIcon("/icons/icon_13.png");
    public static Icon ICON_25 = IconLoader.getIcon("/icons/icon_25.png");

    private ItrulesIcons() {
    }
}
