// This is a generated file. Not intended for manual editing.
package org.siani.itrules.intellij.lang.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;

public class ItrulesVisitor extends PsiElementVisitor {

  public void visitPsiElement(@NotNull ItrulesPsiElement o) {
    visitElement(o);
  }

}
